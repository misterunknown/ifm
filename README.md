# IFM - improved file manager
This is a filemanager based on the "[Easy PHP File Manager](http://epfm.misterunknown.de)". It is also a single file solution which uses HTML, CSS, Javascript and PHP, so it's like a client-server-system where HTML/CSS/Javascript is the client and the PHP API acts as an server. So it is more dynamic and produces less traffic than the EPFM.
The IFM comes with a embedded Version of [jQuery](http://jquery.com) and [CodeMirror](http://codemirror.net).
## developers
written by Marco Dickert [(website)](http://misterunknown.de)
designed by Sebastian Langer [(website)](http://sebastianl.de)
